TlDatLexer is a plugin for Notepad++ which adds extended support for Torchlight data files.

#Features
* Syntax highlighting
* Folding
* Indication of incorrectly formatted values

#Installation
1. Copy 'TlDatLexer.dll' to your 'Notepad++/plugins' or '%APPDATA%/Notepad++/plugins' directory.
2. Copy the 'Config/%THEME%/TlDatLexer.xml' to your 'Notepad++/plugins/Config' or '%APPDATA%/Notepad++/plugins/Config' directory.